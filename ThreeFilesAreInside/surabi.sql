/* Execute these commands step by step*/

/*		Command 1(This will create a database)		*/
 create database surabi;
 
 /*		Command 2(Use the surabi database for next commands)		*/
 use surabi;
 
 /*		Command 3(Create a Bill table. This will store all the orders made by user)		*/
 create table Bill(
 Sequence_Number int NOT NULL auto_increment,
 Bill_date varchar(30),
 No_of_Items int,
 Total_amount int,
 primary key (Sequence_Number)
 );
 
 
 /*		Command 4(Create the menu table. This will store the items available in the restaurant)	*/
 create table Menu(
 Sequence_Number int,
 Item_name varchar(30),
 Item_cost int
 );
 
 /*		Command	5(Create a user table. This will store the credentials of all users)		*/
 create table user(
 User_id int NOT NULL auto_increment,
 User_email varchar(40),
 User_pwd varchar(50),
 primary key(User_id)
 );
 
 
 /*		Command	6(Insert some users into the users table)		*/
 insert into user
 values
 (1,'user11@gmail.com','qwerty'),
 (2,'user12@gmail.com','123456');
 
 
 /*		Command	7(Insert the items available at the restaurant in the menu)*/
  insert into Menu
  value 
  (1,'VadaPav', 40),
  (2,'IdliSambhar',50),
  (3,'VadaSambhar',60),
  (4,'PaniPuri', 20),
  (5,'DahiPuri', 30),
  (6,'BhelPuri',70),
  (7,'Maggie',30),
  (8,'Pasta',60);
  
  /*	Command8 (Create a admin table. This will store the credentials of all admin) */
  create table admin(
 Admin_id int NOT NULL auto_increment,
 Admin_email varchar(40),
 Admin_pwd varchar(50),
 primary key(Admin_id)
 );
  
 /*		Command	9(Insert some admin into the admin table)		*/
 insert into admin
 values
 (1,'admin11@gmail.com','qwerty'),
 (2,'admin12@gmail.com','123456'); 


/*     Command 10(Use below commands to see tables)       */
select * from admin;
select * from bill;
select * from menu;
select * from user;

 
