//This class is used to show all the items in menu to user
package com.firstproject.Restaurants;
import java.sql.ResultSet;
import java.sql.Statement;

public class CustomersDAO {
	public static void getAllItems(Statement stmt) throws Exception{
		//Creating the query as a string
		String str = "select Sequence_number, Item_name, Item_cost from Menu";
		
		//Executing the query and the taking the results in a ResultSet
		ResultSet rs = (stmt).executeQuery(str);
		int rowCount=0;
		System.out.println("Please find the menu below to select the sequence number");
		 
		//Printing all the items present in the menu
		while(rs.next()) {
			int sequence_number = rs.getInt("Sequence_number");
			String Item_name = rs.getString("Item_name");
			int Item_cost = rs.getInt("Item_cost");
			
			System.out.println("Sequence_number ="+sequence_number);
			System.out.println("Item_name="+Item_name);
			System.out.println("Item_cost="+Item_cost);
			System.out.print("\n");
			rowCount++;
		}
	}
}
