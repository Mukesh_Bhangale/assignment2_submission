package com.firstproject.Restaurants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

//This is a class for admin which is implementing Runnable interface
class loginAdmin implements Runnable{
	
	private Connection connection;
	private Statement statement;
	public loginAdmin(Connection connection, Statement statement) {
		super();
		this.connection = connection;
		this.statement = statement;
	}
	
	@Override
	public void run(){
		//Take the Email id from admin as an input
		System.out.println("Please Enter Your Email");
		Scanner sc = new Scanner(System.in);
		String email = sc.nextLine();
		
		PreparedStatement st1, st2;
		boolean check1=false, check2=false;
		String query1;
		try {
			try{
				//Checked weather the email id is valid or not
				query1 = "select * from admin where Admin_email = '"+email+"'";
				st1 = connection.prepareStatement(query1);
				ResultSet rs1 = st1.executeQuery(query1);
				if(rs1.isBeforeFirst()) {
					check1=true;
				}
			}catch(Exception e2) {
				System.out.println("\n"+"Please enter a valid email id. You have been logged out. Please login again :)"+"\n");
				return;	
			}
			
			if(check1) {
					//If the email id is correct then take the password from admin
					System.out.println("Please enter your password."+"\n");
					String password = sc.nextLine();
					try {
						String query2 = "select * from admin where Admin_pwd =  '"+password+"'";
						st2 = connection.prepareStatement(query2);
						ResultSet rs2 = st2.executeQuery(query2);
						if(rs2.isBeforeFirst()) {
							check2 = true;
						}	
					}
					catch(SQLException e1) {
						System.out.println("\n"+"Please enter a valid password. You have been logged out. Please login again :)"+"\n");
						return;
					}
					if(check2) {
						//If the password is correct then show the bills which are generated today
						TodayBills.generateBill(connection);	 
					}else {
						System.out.println("\n"+"Please enter a valid password. You have been logged out. Please login again :)"+"\n");
						return;
					}
					
			}else {
				System.out.println("\n"+"Please enter a valid email id. You have been logged out. Please login again :)"+"\n");
				return;
			}
			//This will give the total sale for this month	
			MonthSale.sale(connection);
					
		}catch(Exception e4) {
			e4.printStackTrace();
		}
	}
}
