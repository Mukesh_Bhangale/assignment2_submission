package com.firstproject.Restaurants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

public class MonthSale {
	public static void sale(Connection connection) throws Exception{
		//Show the sale for this month to the admin
		String str1 = "SELECT sum(Total_amount) as total_amount FROM Bill WHERE month(Bill_date) = MONTH(NOW()) AND year(Bill_date) = YEAR(NOW())";
		PreparedStatement  st3 = connection.prepareStatement(str1);
		ResultSet rs1 = (st3).executeQuery(str1);
		while(rs1.next()){
			int Total_amount = rs1.getInt("total_amount");
			System.out.println("Total sale for this month is "+ rs1.getInt("total_amount"));
			System.out.println("\n");
		}
		
		//User can logout by entering any key
		System.out.println("Press any key to logout");
		Scanner sc = new Scanner(System.in);
		String log =  sc.nextLine();
		return;
	}
}
