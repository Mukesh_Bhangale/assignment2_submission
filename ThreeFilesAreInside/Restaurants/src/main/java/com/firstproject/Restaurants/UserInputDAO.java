//This class is created to show the order for user, show the final bill 

package com.firstproject.Restaurants;
import java.sql.*;
import java.util.*;
import java.io.*;
import java.time.*;
import java.time.format.DateTimeFormatter;
public class UserInputDAO {
	public static void printAllItems(Statement stmt, Connection conn) throws SQLException{
		int finalBill = 0;  //Variable is created to calculate the final bill
		 
		Scanner scan = new Scanner(System.in);
		String user_input = scan.nextLine();       //Sequence number of items are taken in a single line 
		String[] stringsArray = user_input.split(" ");
		int[] intArray = new int[stringsArray.length];
		
		for(int i=0; i<stringsArray.length; i++) {
			intArray[i]=Integer.parseInt(stringsArray[i]);
			if(intArray[i]<1 || intArray[i]>8) {
				System.out.println("\n"+"Please enter the sequence numbers from 1 to 8 because there are only 8 items available. You have been logged out. Please login again."+"\n");
				return;
			}
			
			//A query is created in a string which will be helpful in calculating the final bill
			String query = "select Item_cost from menu where Sequence_number = "+Integer.toString(intArray[i]);
			ResultSet rs = (stmt).executeQuery(query);
			if(rs.next()) {
				int Item_cost = rs.getInt("Item_cost");
				finalBill = finalBill + Item_cost;   //Final bill is calculated by adding the cost of all items entered by user.
			}
				
		}
		
		//Date of using this application by user is inserted into the database using LocalDate object
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate today = LocalDate.now( ZoneId.of( "Asia/Kolkata" ) ) ;
		dtf.format(today);
		String output = today.toString();   //This string will store the date
		
		
		//Input from user is stored in the database for future calculations
		//Bill date, No of items ordered by user and total amount is stored in the database
		String query1 = "insert into Bill(Bill_date, No_of_items, Total_amount) value (?,?,?)";
		PreparedStatement  st = conn.prepareStatement(query1);
		st.setString(1,output);
		st.setInt(2,stringsArray.length);
		st.setInt(3,finalBill);
		st.executeUpdate();
		
		//Final bill is printed on the screen of user
		System.out.println("\n");
		System.out.println(finalBill+" is your finalBill ");
		System.out.println("\n");
		
		//Now user can logout by entering any key.
		System.out.println("Press any key to logout");
		String log = scan.nextLine();
		
		
	}

	 

}
