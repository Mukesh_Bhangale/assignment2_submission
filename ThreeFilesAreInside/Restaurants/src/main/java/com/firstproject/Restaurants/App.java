package com.firstproject.Restaurants;
import java.sql.*;
import java.util.*;
import java.util.regex.Pattern;
import java.io.*;
import java.lang.*;

public class App 
{
    public static void main(String[] args)
    {
    	try{
    		//Loading the driver manager
    		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/surabi","root","123654aaa");
    		//Creating the connection from database
    		java.sql.Statement stmt = conn.createStatement();
    		int input;
    		do {
    			//Asking, weather a user or an admin is using this application
    			System.out.println("Please Enter 1 if you are a user");
    			System.out.println("Please Enter 2 if you are an admin");
    			Scanner scan = new Scanner(System.in);
  
    			//This try/catch block will be implemented when the input is a character other than integer
    			try{
    				input=scan.nextInt();
    				scan.nextLine();
    			} catch(Exception e){
    				System.out.println("Please enter either 1 or 2"+"\n");
    				System.out.println("\n");
    				continue;
    			}
    			
    			//If the input is integer and it is not 1 and 2, show an error message 
    			if(input!=1 && input!=2){
    				System.out.println("Please enter either 1 or 2"+"\n");
    				continue;
    			}
    			
    			//If the input is 1 that means the user is want to access the application
    			else if(input==1){
    				loginUser login = new loginUser(conn, stmt, new CustomersDAO());
    				Thread UserThread= new Thread(login);
    				UserThread.start();
    				UserThread.join();
    			}
    			
    			//If the input is 2 that means the admin wants to access the application 
    			else if(input==2) {
    				loginAdmin login_admin = new loginAdmin(conn, stmt);
    				Thread AdminThread = new Thread(login_admin);
    				AdminThread.start();
    				AdminThread.join();
    			}
    		}while(true);
    	}catch(Exception ex) {
    		ex.printStackTrace();
    	}
    }
}
