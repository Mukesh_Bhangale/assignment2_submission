package com.firstproject.Restaurants;
import java.util.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TodayBills {
	public static void generateBill(Connection connection) throws Exception{
	
		String str = "select No_of_Items, Total_amount from Bill where Bill_date = Curdate()";
		
		PreparedStatement st = connection.prepareStatement(str);
		ResultSet rs = null;
		try {
			rs = st.executeQuery(str);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		int rowCount=0, i=1;
		System.out.println("\n"+"These are the bills which are generated today");
		System.out.println("\n");
		
		try {
			while(rs.next()){
				int No_of_Items = 0, Total_amount = 0;;
				No_of_Items = rs.getInt("No_of_Items");
				Total_amount = rs.getInt("Total_amount");
				 
				System.out.println("Bill Number : "+ i);
				System.out.println("No_of_Items = "+No_of_Items);
				System.out.println("Total_amount = "+Total_amount);
				System.out.print("\n");
				rowCount++;
				i++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
