package com.firstproject.Restaurants;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLSyntaxErrorException;
import java.sql.Statement;
import java.util.Scanner;

class loginUser implements Runnable{
	private Connection connection; 
	private Statement statement; 
	private CustomersDAO customersDAO;
	public loginUser(Connection connection, Statement statement, CustomersDAO customersDAO){
		super();
		this.connection = connection; 
		this.statement = statement;
		this.customersDAO = customersDAO;
	}

	//This method is called from the main method.
	@Override
	public void run(){
		//User email as a input is taken
		System.out.println("Please Enter Your Email");
		Scanner sc = new Scanner(System.in);
		String email = sc.nextLine();
		
		PreparedStatement st1, st2;
		boolean check1=false, check2=false;
		String query1;
		try {
			//Checked weather the email id is valid or not
			try{
				query1 = "select * from user where User_email = '"+email+"'";
				st1 = connection.prepareStatement(query1);
				ResultSet rs1 = st1.executeQuery(query1);
				if(rs1.isBeforeFirst()) {
					check1=true;
				}
			}catch(SQLSyntaxErrorException e2) {
				System.out.println("please enter a valid email id"+"\n");
				e2.printStackTrace();
				return;
			}
			
			if(check1) {
					//If the email id is correct then take the password from user
					System.out.println("Please enter your password" + "\n");
					String password = sc.nextLine();
					try {
						String query2 = "select * from user where User_pwd = '"+password+"'";
						st2 = connection.prepareStatement(query2);
						ResultSet rs2 = st2.executeQuery(query2);
						if(rs2.isBeforeFirst()) {
							check2 = true;
						}	
					}
					catch(SQLException e1) {
						System.out.println("Please enter a valid password"+"\n");
						return;
					}
					
					//If the password is correct then take the order from user in a single line
					if(check2) {
						System.out.println("\n"+"Please select the items by entering their sequence number in one line"+"\n");
						
						//Get all the items from Database to show the user as a menu
						try {
							CustomersDAO.getAllItems(statement);
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						//Take the order from user according to the sequence number of each item
						UserInputDAO.printAllItems(statement,connection);
					}else {
						System.out.println("\n"+"Please enter a valid password. You have been logged out. Please login again :)"+"\n");
					}
				}else {
					    System.out.println("\n"+"please enter a valid email id. You have been logged out. Please login again :)"+"\n");
				} 
		} catch (SQLException e3) {
			 
			e3.printStackTrace();
		}
	}
}
