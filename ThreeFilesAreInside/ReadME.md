Instructions :

Run App.java File as a java application to start the application.

1. Name of the database is surabi and 4 tables are created in this database which are user, Bills, Menu and admin.
2. SQL connection should be checked before running the application.
3. SQL file should be loaded before running the application.
4. Initially the console will ask if you are a user or an admin. Please press 1 if you are a user and 2 if your are an admin.
5. I have already mentioned some users in the users table. If the credentials matches, then only the user will be allowed to login.
6. Credentials for login as an admin are:  
 **email_id: admin11@gmail.com,           password: qwerty** and
 **email_id: admin12@gmail.com,	       password: 123456**


7. Credentials for login as an user are:

 **email_id: user11@gmail.com       password: qwerty** and 
 **email_id: user12@gmail.com	  password: 123456**

8. When entering the sequence number for items, please add them in a single with a space between them like 1 2 3.
9. For logging out, Please press any key after the console pop's up with message "Press any key to logout"
